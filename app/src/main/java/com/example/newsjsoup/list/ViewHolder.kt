package com.example.newsjsoup.list

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.ListFragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.newsjsoup.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.details_news_fragment.view.*
import kotlinx.android.synthetic.main.item.view.*

class ViewHolder(view: View):RecyclerView.ViewHolder(view) {

    private val title:TextView = view.row_tv_title
    private val description:TextView = view.row_tv_description
    private val additionalInfo:TextView = view.row_tv_additional_info
    private val image:ImageView = view.row_img
    private var link = ""

    init {
        itemView.setOnClickListener{
            itemView.findNavController().navigate(ListNewsFragmentDirections.actionListNewsFragmentToDetailsNewsFragment(link))
        }
    }

    fun bind(news: News) {
        title.text = news.title
        description.text = news.description
        Picasso.with(itemView.context)
            .load(news.linkImage)
            .into(image)
        additionalInfo.text = news.additionalInfo
        link = news.linkDetails
    }
}