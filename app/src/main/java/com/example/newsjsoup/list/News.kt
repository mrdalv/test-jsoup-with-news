package com.example.newsjsoup.list

class News (
    val title:String,
    val description:String,
    val linkImage:String,
    val additionalInfo:String,
    val linkDetails: String
)